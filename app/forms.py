from django import forms


class InputForm(forms.Form):
    attrs = {
        'type': 'text',
        'class': 'form-control',
        'placeholder': 'Type Here!'
    }
    command = forms.CharField(
        label='Hello, what do you want me to do?', widget=forms.TextInput(attrs=attrs))
