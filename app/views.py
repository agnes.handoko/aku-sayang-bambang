from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import InputForm
import json

response = {}
PATH_TO_TXT = "D:\\s1\\Sysprog\\project\\webinterface\\input.txt"
# Create your views here.


def homepage(request):
    response = {}
    if request.method == 'POST':
        form = InputForm(request.POST)
        if (form.is_valid()):
            write_file(request.POST['command'])
    form = InputForm()
    response['form'] = form
    response['input'] = read_file
    return render(request, "bambang.html", response)


"""write_file ini dipake nanti buat nampung hasil POST user dari web interface"""


def write_file(text):
    file = open(PATH_TO_TXT, "w")

    file.write(text)

    file.close()


"""read_file ini dipake buat bot1.py untuk ambil pertanyaan user"""


def read_file():
    inp = ""
    open_file = open(PATH_TO_TXT, "r+")
    file = open_file.readlines()
    try:
        inp = file[0]
    # delete isinya
    except IndexError:
        inp = ""
    return inp
